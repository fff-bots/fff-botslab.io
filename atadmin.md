# AtAdminBot 2.1
by Abfelbaum

## What is this?
[@AtAdminBot](https://t.me/AtAdminBot) is a simple bot to notify chat admins when someone writes @admin @admins or /admins. An admin will only get a notification, if they started the chat with the bot before.

[Changelog](https://git.thevillage.chat/Abfelbaum/atadminbot/blob/master/changelog.md)

## Installation
open the environment.env file and replace your token with the example token

after that:

on windows: execute `build.cmd`

on linux: `./build.sh`

then:
### docker-compose
`docker-compose up -d`

if you want to compile the sources by yourself:
comment out the `image:` in `docker-compose.yml` and uncomment the `build:`

### docker
replace ``name`` with your preferred container name

#### precompiled
``docker run registry.git.thevillage.chat/abfelbaum/atadminbot``
#### self compile
build: ``docker build -t name .``

run: ``docker run --rm -it name:latest``

### from source
build: `dotnet publish -c Release`

run: `dotnet AtAdminBot/bin/Release/netcoreapp2.2/publish/AtAdminBot.dll`

## Bot commands
```
admins - notifies all admins that started the bot
privacy - shows which data the bot collects
about - information about the bot
```

## How to update
go in the directory

pull the latest src: ``git pull``

on windows: execute `build.cmd`

on linux: `./build.sh`


then:

### docker-compose
rebuild container: ```docker-compose build```

and bring it back up: ```docker-compose down && docker-compose up -d```

### docker
stop container: ```docker stop name```

remove container: ```docker rm name```

build container: ```docker build -t name .```

and start it again: ``docker run --rm -it name:latest``

### from source
stop the old version

build: `dotnet publish -c Release`

run: `dotnet AtAdminBot/bin/Release/netcoreapp2.2/publish/AtAdminBot.dll`
