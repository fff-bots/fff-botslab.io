# FFF Inlinebot
## Beschreibung
Ein Bot zur Verwaltung von den Telegram Gruppen. Man gibt @fffinlinebot ein, und es erscheint eine Liste,
mit möglichen Nachrichten. Das ist Zum Beispiel eine Anti-Caps Nachricht.
## Gehostete Instanz
https://t.me/fffinlinebot
## Source Code
https://gitlab.com/fff-bots/fff-inline-bot