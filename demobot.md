# FFF Demobot
## Beschreibung
Dieser Bot sendet die Demos für kommenden Freitag per Telegram and den Nutzer.
## Gehostete Instanz
https://t.me/fffdemobot
## Befehle
/demos - zeigt die demos für kommenden Freitag an.
## Source Code
https://gitlab.com/fff-bots/fffdemobot
